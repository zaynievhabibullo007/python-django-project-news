# Generated by Django 4.2.2 on 2023-06-21 02:37

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0025_alter_news_publish_time'),
    ]

    operations = [
        migrations.AlterField(
            model_name='news',
            name='publish_time',
            field=models.DateTimeField(default=datetime.datetime(2023, 6, 21, 2, 37, 48, 753735, tzinfo=datetime.timezone.utc)),
        ),
    ]
