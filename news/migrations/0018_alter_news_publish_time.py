# Generated by Django 4.2.1 on 2023-06-18 02:55

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0017_alter_news_publish_time'),
    ]

    operations = [
        migrations.AlterField(
            model_name='news',
            name='publish_time',
            field=models.DateTimeField(default=datetime.datetime(2023, 6, 18, 2, 55, 7, 702512, tzinfo=datetime.timezone.utc)),
        ),
    ]
