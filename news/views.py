from typing import Any
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.models import User
from django.db.models.query import QuerySet
from django.http import HttpResponse
from django.urls import reverse_lazy
from django.db.models import Q
from django.views.generic import DetailView, TemplateView, ListView, UpdateView, DeleteView, CreateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, get_object_or_404
from .forms import ContactForm, CommentForm
from .models import News, Category
from config.custom_permissions import OnlyLoggedSuperUser


def contactView(request):
    form = ContactForm(request.POST or None)
    if request.method == "POST" and form.is_valid():
        form.save()
        return HttpResponse("<h2> Biz bilan bog'langaniz uchun raxmat. </h2>")
    context = {
        'form': form

    }
    return render(request, 'news/contact.html', context)


def _404_View(request):
    context = {

    }
    return render(request, 'news/404.html', context)


def newslistview(request):
    news_list = News.objects.all().order_by('-publish_time').filter(status=News.Status.Published)
    context = {
        'news_list': news_list
    }
    return render(request, 'news/news_list.html', context)


def newsdetailview(request, news):
    news = get_object_or_404(News, slug=news, status=News.Status.Published)
    comments = news.comments.filter(active=True)
    comment_count = comments.count()
    if request.method == 'POST':
        comment_form = CommentForm(data=request.POST)
        if comment_form.is_valid():
            new_comment = comment_form.save(commit=False)
            new_comment.news = news
            new_comment.user = request.user
            new_comment.save()
            comment_form = CommentForm()
    else:
        comment_form = CommentForm()
    context = {
        'news': news,
        'comments': comments,
        'comment_form': comment_form,
        'comment_count': comment_count

    }
    return render(request, 'news/news_detail.html', context)


class NewsDetailView(DetailView):
    model = News
    template_name = 'news/news_detail.html'

    def get_context_data(self, **kwargs):
        context = super(NewsDetailView, self).get_context_data(**kwargs)


def homePageView(request):
    news_list = News.objects.all().order_by('-publish_time').filter(status=News.Status.Published)[:5]
    categories = Category.objects.all()
    local_list = News.objects.all().filter(status=News.Status.Published).filter(category__name="Mahalliy").order_by(
        '-publish_time')[:6]
    context = {
        'news_list': news_list,
        'categories': categories,
        'local_list': local_list,

    }
    return render(request, 'news/home.html', context)


class HomePageView(ListView):
    model = News
    template_name = 'news/home.html'
    context_object_name = 'news'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['categories'] = Category.objects.all()
        context['news_list'] = News.objects.all().order_by('-publish_time').filter(status=News.Status.Published)[:5]
        context['local_list'] = News.objects.all().filter(status=News.Status.Published).filter(
            category__name="Mahalliy").order_by('-publish_time')[:6]
        context['sport_list'] = News.objects.all().filter(status=News.Status.Published).filter(
            category__name="Sport").order_by('-publish_time')[:6]
        context['xorij_list'] = News.objects.all().filter(status=News.Status.Published).filter(
            category__name="Jahon").order_by('-publish_time')[:6]
        context['texnologiya_list'] = News.objects.all().filter(status=News.Status.Published).filter(
            category__name="Texnologiya").order_by('-publish_time')[:6]

        return context


class LocalPageView(ListView):
    model = News
    template_name = "news/local.html"
    context_object_name = 'local_list'

    def get_queryset(self):
        news = self.model.objects.all().filter(status=News.Status.Published).filter(category__name="Mahalliy")
        return news


class TechnalogyPageView(ListView):
    model = News
    template_name = "news/technology.html"
    context_object_name = 'texnologiya_list'

    def get_queryset(self):
        news = self.model.objects.all().filter(status=News.Status.Published).filter(category__name="Texnologiya")
        return news


class SportPageView(ListView):
    model = News
    template_name = "news/sport.html"
    context_object_name = 'sport_list'

    def get_queryset(self):
        news = self.model.objects.all().filter(category__name="Sport")
        return news


class ForiegnPageView(ListView):
    model = News
    template_name = "news/foriegn.html"
    context_object_name = 'xorij_yangiliklari'

    def get_queryset(self):
        news = self.model.objects.all().filter(status=News.Status.Published, category__name="Jahon").order_by(
            '-publish_time')
        return news


class NewsUpdateView(OnlyLoggedSuperUser, UpdateView):
    model = News
    fields = ['title', 'body', 'image', 'category', 'status']
    template_name = 'crud/news_edit.html'


class NewsDeleteView(OnlyLoggedSuperUser, DeleteView):
    model = News
    template_name = 'crud/news_delete.html'
    success_url = reverse_lazy('home_page')


class NewsCreateView(OnlyLoggedSuperUser, CreateView):
    model = News
    template_name = 'crud/create_page.html'
    fields = ['title', 'title_uz', 'title_en', 'title_ru', 'slug', 'body', 'body_uz', 'body_en', 'body_ru', 'image',
              'category', 'status']


class ContactPageView(OnlyLoggedSuperUser, TemplateView):
    template_name = 'news/contact.html'

    def get(self, request, *args, **kwargs):
        form = ContactForm()
        context = {
            'form': form
        }
        return render(request, 'news/contact.html', context)

    def post(self, request, *args, **kwargs):
        form = ContactForm(request.POST)
        if request.method == 'POST' and form.is_valid():
            form.save()
            return HttpResponse("<h2> Biz bilan bog'langaniz uchun raxmat.</h2>")
        context = {
            'form': form
        }
        return render(request, 'news/contact.html', context)


@login_required
@user_passes_test(lambda u: u.is_superuser)
def admin_page_view(request):
    admin_user = User.objects.filter(is_superuser=True)
    context = {
        'admin_user': admin_user
    }
    return render(request, 'pages/admin_page.html', context)


class SearchNewsView(ListView):
    model = News
    template_name = 'news/search_news.html'
    context_object_name = 'all_news'

    def get_queryset(self):
        query = self.request.GET.get('q')
        return News.objects.filter(Q(title__icontains=query) | Q(body__icontains=query))
