from modeltranslation.translator import register, TranslationOptions, translator

from news.models import News, Category


class NewsTranslationOptions(TranslationOptions):
    fields = ('title', 'body')


class CategoryTranslationOptions(TranslationOptions):
    fields = ('name',)


translator.register(News, NewsTranslationOptions)
translator.register(Category, CategoryTranslationOptions)
