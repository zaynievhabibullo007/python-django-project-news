from django.urls import path
from news.views import (_404_View, ContactPageView, newsdetailview, newslistview, \
                        HomePageView, TechnalogyPageView, SportPageView, ForiegnPageView, LocalPageView, \
                        NewsUpdateView, NewsDeleteView, NewsCreateView, admin_page_view, SearchNewsView)

urlpatterns = [
    path('', HomePageView.as_view(), name='home_page'),
    path('search/',SearchNewsView.as_view(), name='search'),
    path('detail/<slug:news>', newsdetailview, name='detail_page'),
    path('edit/<slug:pk>/', NewsUpdateView.as_view(), name='news_edit_page'),
    path('delete/<slug:pk>/', NewsDeleteView.as_view(), name='news_delete_page'),
    path('create/', NewsCreateView.as_view(), name='create_news_page'),
    path('technology/', TechnalogyPageView.as_view(), name='technology_news_page'),
    path('sport/', SportPageView.as_view(), name='sport_news_page'),
    path('foreign/', ForiegnPageView.as_view(), name='foreign_news_page'),
    path('local/', LocalPageView.as_view(), name='local_news_page'),
    # ortiqcha
    path('news_all/', newslistview, name='news_lists'),
    path('contact_uc/', ContactPageView.as_view(), name='contact_page'),
    path('error_404/', _404_View, name='_404_page'),
    path('admin_page/', admin_page_view, name='admin_page'),

]
